// spec.js
describe('CoInspect Demo App', function() {
//   var firstNumber = element(by.model('first'));
//   var secondNumber = element(by.model('second'));
//   var goButton = element(by.id('gobutton'));
//   var latestResult = element(by.binding('latest'));

  beforeEach(function() {
    browser.get('http://127.0.0.1:3000/');
  });

//   it('should have a title', function() {

//       browser.waitForAngular();

//     var title = browser.getTitle();
//     title.then(function(webpagetitle)
//     {
//         console.log(" Title Length = " + browser.getTitle());
//         // expect(browser.getTitle().length > 0);
//     })

    
//   });

    it('Inspection list should be > 0', function() {
        // var history = element.all(by.repeater('result in memory'));

        // browser.wait(function() {
        //     return element(by.id('inspectionlist')).isPresent();
        // }, 5000)
        // .then( function() {

        // })


        // var inspectionlist = element(by.id('inspectionlist'));

        // expect(inspectionlist.count()).toBe­Gre­ate­rTh­an(2);

        var EC = protractor.ExpectedConditions;

        var e = element(by.id('inspectionlist'));
        browser.wait(EC.presenceOf(e), 10000);

        var insp = element.all(by.repeater('inspection in inspections')); 

        // expect(insp.count()).toBeLessThan(3);
        expect(insp.count()).toBeGreaterThan(0);



        // var insp = element.all(by.repeater('inspection in inspections'));
        // console.log(" items = " + insp.count());

    });

    
    it('Test Create Inspection', function() {

        var EC = protractor.ExpectedConditions;

        var createNewLink = element(by.linkText('Create New'));
        browser.wait(EC.presenceOf(createNewLink), 10000);

        createNewLink.click();

        var propTextbox = element(by.id('property')).sendKeys('New property test');

        var select = element(by.model('inspection.checklist'));

        element(by.model('inspection.checklist')).$('[value="string:Fire Safety"]').click();
        element(by.model('inspection.inspector')).$('[value="string:Sherlock Holmes"]').click();
        element(by.model('inspection.status')).$('[value="string:NEW"]').click();

        element(by.buttonText('SAVE')).click();
        element(by.linkText('Home')).click();

        var e = element(by.linkText('New property test'));
        browser.wait(EC.presenceOf(e), 5000);

        expect(e.isPresent()).toBe(true);

    });



//   it('should add one and two', function() {
//     firstNumber.sendKeys(1);
//     secondNumber.sendKeys(2);

//     goButton.click();

//     expect(latestResult.getText()).toEqual('3');
//   });

//   it('should add four and six', function() {
    
//     firstNumber.sendKeys(6);
//     secondNumber.sendKeys(4);

//     goButton.click();

//     expect(latestResult.getText()).toEqual('10');
//   });
});

// function waitForPromiseTest(promiseFn, testFn) {
//   browser.wait(function () {
//     var deferred = protractor.promise.defer();
//     promiseFn().then(function (data) {
//       deferred.fulfill(testFn(data));
//     });
//     return deferred.promise;
//   });
// }


var selectDropdownbyNum = function ( source, optionNum ) {
  if (optionNum){
    var options = source.element(by.tagName('option'))   
    browser.wait(EC.presenceOf(options), 3000);
    options[optionNum].click();
  }
};